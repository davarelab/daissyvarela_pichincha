import { AutoMap } from '@automapper/classes';

export class OrganizationDto {
  @AutoMap()
  id: number;
  @AutoMap()
  name: string;
  @AutoMap()
  status: number;
}
