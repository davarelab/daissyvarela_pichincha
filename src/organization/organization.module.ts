import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrganizationController } from './organization.controller';
import { OrganizationService } from './organization.service';
//import { HeroRepository } from '../utility/in-memory-db';
import { Organization } from './organization.entity';

@Module({
  controllers: [OrganizationController],
  providers: [OrganizationService /* HeroRepository*/],
  imports: [TypeOrmModule.forFeature([Organization])],
})
export class OrganizationModule {}
