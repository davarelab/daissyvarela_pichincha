import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { InjectMapper } from '@automapper/nestjs';
import { Mapper } from '@automapper/core';
import { Organization } from './organization.entity';
import { OrganizationDto } from './organization.dto';

@Injectable()
export class OrganizationService {
  constructor(
    @InjectRepository(Organization)
    private organizationRepository: Repository<Organization>,
    @InjectMapper() private readonly classMapper: Mapper,
  ) {}

  async getAllOrganizations(): Promise<Organization[]> {
    return this.classMapper.mapArrayAsync(
      await this.organizationRepository.find(),
      Organization,
      OrganizationDto,
    );
  }

  async saveOrganization(
    organization: OrganizationDto,
  ): Promise<OrganizationDto> {
    let response;

    if (null !== organization.id) {
      await this.organizationRepository.update(organization.id, organization);
      response = await this.organizationRepository.findOneBy({
        id: organization.id,
      });
    } else {
      response = this.organizationRepository.save(organization);
    }

    return this.classMapper.map(response, Organization, OrganizationDto);
  }

  async deleteOrganization(id: number): Promise<DeleteResult> {
    this.organizationRepository.findOneByOrFail({ id: id });
    return await this.organizationRepository.delete(id);
  }
}
