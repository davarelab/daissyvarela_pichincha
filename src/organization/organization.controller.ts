import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { OrganizationDto } from './organization.dto';
import { IntPipe } from '../utility/int.pipe';

@Controller('organization')
export class OrganizationController {
  constructor(private organizationService: OrganizationService) {}

  @Get()
  getAllOrganizations(): Promise<OrganizationDto[]> {
    return this.organizationService.getAllOrganizations();
  }

  @Post()
  saveOrganizations(
    @Body() organization: OrganizationDto,
  ): Promise<OrganizationDto> {
    return this.organizationService.saveOrganization(organization);
  }

  @Put(':id')
  updateOrganizations(
    @Param('id', new IntPipe()) id: number,
    @Body() organization: OrganizationDto,
  ): Promise<OrganizationDto> {
    if (id === organization.id) {
      return this.organizationService.saveOrganization(organization);
    } else {
      return null;
    }
  }

  @Delete(':id')
  deleteOrganizations(@Param('id', new IntPipe()) id: number) {
    return this.organizationService.deleteOrganization(id);
  }
}
