import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { OrganizationModule } from './organization/organization.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AutomapperModule } from '@automapper/nestjs';
import { classes } from '@automapper/classes';

@Module({
  imports: [
    OrganizationModule,
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot({
      type: 'cockroachdb',
      /*host: 'free-tier11.gcp-us-east1.cockroachlabs.cloud',
      port: 26257,
      username: 'daissyvarela',
      password: 'XfNnwD0-L49Jx31dM298yw',
      database: 'defaultdb',
      entities: [Organization],
      synchronize: true,*/
      url: 'postgresql://daissyvarela:XfNnwD0-L49Jx31dM298yw@free-tier11.gcp-us-east1.cockroachlabs.cloud:26257/defaultdb?sslmode=verify-full&options=--cluster%3Dbowing-bull-2722',
    }),
    AutomapperModule.forRoot({
      strategyInitializer: classes(),
    }),
    OrganizationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
