-- Execute only once
-- cat dbinit.sql | cockroach sql --url "postgresql://daissyvarela:XfNnwD0-L49Jx31dM298yw@free-tier11.gcp-us-east1.cockroachlabs.cloud:26257/defaultdb?sslmode=verify-full&options=--cluster%3Dbowing-bull-2722"
--
DROP TABLE metrics;
DROP TABLE repository;
DROP TABLE tribe;
DROP TABLE organization;

CREATE TABLE organization (
    id_organization INTEGER NOT NULL PRIMARY KEY,
    name varchar(50) NOT NULL,
    status INTEGER NOT NULL
);

CREATE TABLE tribe (
    id_tribe INTEGER NOT NULL PRIMARY KEY,
    id_organization INTEGER,
    name varchar(50) NOT NULL,
    status INTEGER NOT NULL,
    FOREIGN KEY (id_organization) REFERENCES organization(id_organization)
);

CREATE TABLE repository (
    id_repository INTEGER NOT NULL PRIMARY KEY,
    id_tribe INTEGER,
    name varchar(50) NOT NULL,
    state varchar(1) NOT NULL,
    create_time DATE NOT NULL,
    status varchar(1) NOT NULL,
    FOREIGN KEY (id_tribe) REFERENCES tribe(id_tribe)
);

CREATE TABLE metrics (
    id_repository INTEGER NOT NULL,  
    coverage REAL NOT NULL,
    bugs INTEGER NOT NULL,
    vulnerabilities INTEGER NOT NULL,
    hotspot INTEGER NOT NULL,
    code_smells INTEGER NOT NULL,
    PRIMARY KEY (id_repository),
    FOREIGN KEY (id_repository) REFERENCES repository (id_repository)
);